## About this project

This sample project is built with Angular 11 and NestJS. Some topics I tried to demonstrate in this app:

- Using ChartJS for creating graphs of different types
- Responding to user input and updating graphs
- Creating re-usable components
- Parent/child components and sending data between
- Using TailwindCSS for styling and making app responsive
- Setting up a restful layer in NestJS (mocked fake responses in controllers)
- Generating components using the Angular/Nest CLI. You obviously won't see that, but it was done :)
- Properly typing Typescript vs. using 'any'

## How to launch

1. Clone the repository
    1. `git clone git@bitbucket.org:joetito1/optiv-sample.git`
2. Install Node and Angular CLI (guessing you have this already)
3.  Serve up Angular
    1. `cd client`
    2. `ng serve`
4.  Start NestJS
    1. `cd server`
    2.  `npm start`
5. Application will be available here: [http://localhost:4200](http://localhost:4200)

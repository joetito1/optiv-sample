import { Controller, Get, Param } from '@nestjs/common';
import { InterviewCountMetric } from 'src/models/interview-count-metric';
import { QualityMetric } from 'src/models/quality-metric';

@Controller('metrics')
export class MetricsController {
  @Get('hires/:days')
  numberOfHires(@Param() params): number {
    console.log('Request new hires metrics for last ' + params.days + ' days.');

    return Math.floor(Math.random() * 100);
  }

  @Get('quality/:days')
  candidateQuality(@Param() params): QualityMetric {
    console.log(
      'Request quality of hires metrics for last ' + params.days + ' days.',
    );

    return new QualityMetric(
      Math.floor(Math.random() * 10),
      Math.floor(Math.random() * 50),
      Math.floor(Math.random() * 20),
    );
  }

  @Get('interviews/:days')
  interviewCount(@Param() params): InterviewCountMetric {
    console.log(
      'Request interview count metrics for last ' + params.days + ' days.',
    );

    const interviewCountMetric: InterviewCountMetric = new InterviewCountMetric(
      [],
      [],
    );

    for (let i = 0; i < params.days; i++) {
      const date: Date = new Date();
      date.setDate(date.getDate() - i);

      interviewCountMetric.dates.push(date.getMonth() + '/' + date.getDay());
      interviewCountMetric.counts.push(Math.floor(Math.random() * 20));
    }

    return interviewCountMetric;
  }
}

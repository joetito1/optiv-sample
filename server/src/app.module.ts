import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MetricsController } from './metrics/metrics.controller';
import { InterviewsController } from './interviews/interviews.controller';

@Module({
  imports: [],
  controllers: [AppController, MetricsController, InterviewsController],
  providers: [AppService],
})
export class AppModule {}

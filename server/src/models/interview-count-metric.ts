export class InterviewCountMetric {
  dates: string[];
  counts: number[];

  constructor(dates: string[], counts: number[]) {
    this.dates = dates;
    this.counts = counts;
  }
}

import { Controller, Get } from '@nestjs/common';
import { Interview } from 'src/models/interview';
import { Person } from 'src/models/person';

@Controller('interviews')
export class InterviewsController {
  @Get('upcoming')
  numberOfHires(): Interview[] {
    console.log('Requesting upcoming interviews');

    return [
      new Interview(
        new Person('Kevin', 'Bacon', '123-456-7432'),
        new Person('Joe', 'Tito', '123-456-7432'),
        '4/1/2021',
        '2:30pm',
      ),
      new Interview(
        new Person('Dana', 'Morgan', '123-456-7432'),
        new Person('Kevin', 'Smith', '123-456-7432'),
        '4/2/2021',
        '10:00am',
      ),
      new Interview(
        new Person('Kyle', 'Johnson', '123-456-7432'),
        new Person('Edward', 'Baker', '123-456-7432'),
        '4/5/2021',
        '3:00pm',
      ),
    ];
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewHireCountComponent } from './new-hire-count.component';

describe('NewHireCountComponent', () => {
  let component: NewHireCountComponent;
  let fixture: ComponentFixture<NewHireCountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewHireCountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewHireCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

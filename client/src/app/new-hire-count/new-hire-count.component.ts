import { Component, OnInit } from '@angular/core';
import { MetricService } from '../metric.service';

@Component({
  selector: 'app-new-hire-count',
  templateUrl: './new-hire-count.component.html',
  styleUrls: ['./new-hire-count.component.css']
})
export class NewHireCountComponent implements OnInit {

  newHireCount = 0;

  constructor(private metricService: MetricService) { }

  ngOnInit(): void {
    this.getNewHireCount(30);
  }

  // Get the number of new hires and update
  getNewHireCount(days: number): void {
    this.metricService.getHireCount(days).subscribe(newHireCount => {
      this.newHireCount = newHireCount;
    });
  }

  // Respond to the user changing the time frame window by pulling new metrics
  onChangedTimeWindow(timeWindow: number): void {
    this.getNewHireCount(timeWindow);
  }

}

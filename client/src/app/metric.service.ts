import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { QualityMetric } from './models/quality-metric';
import { InterviewCountMetric } from './models/interview-count-metric';

@Injectable({
  providedIn: 'root'
})
export class MetricService {

  metricsBaseUrl = '/api/metrics/';

  constructor(private http: HttpClient) { }

  getInterviewCount(days: number): Observable<InterviewCountMetric> {
    return this.http.get<InterviewCountMetric>(`${this.metricsBaseUrl}interviews/${days}`);
  }

  getCandidateQuality(days: number): Observable<QualityMetric> {
    return this.http.get<QualityMetric>(`${this.metricsBaseUrl}quality/${days}`);
  }

  getHireCount(days: number): Observable<number> {
    return this.http.get<number>(`${this.metricsBaseUrl}hires/${days}`);
  }
}

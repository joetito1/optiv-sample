import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Interview } from './models/interview';

@Injectable({
  providedIn: 'root'
})
export class InterviewService {

  metricsBaseUrl = '/api/interviews/';

  constructor(private http: HttpClient) { }

  getUpcomingInterviews(): Observable<Interview[]> {
    return this.http.get<Interview[]>(`${this.metricsBaseUrl}upcoming`);
  }
}

import { Person } from './person';

export class Interview {
    recruiter: Person;
    candidate: Person;
    date: string;
    time: string;

    constructor(recruiter: Person, candidate: Person, date: string, time: string) {
        this.recruiter = recruiter;
        this.candidate = candidate;
        this.date = date;
        this.time = time;
    }
}

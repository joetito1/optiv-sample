export class QualityMetric {

    high: number;
    average: number;
    low: number;

    constructor(high: number, average: number, low: number) {
        this.high = high;
        this.average = average;
        this.low = low;
    }
}

import { Component, OnInit } from '@angular/core';
import { InterviewService } from '../interview.service';
import { Interview } from '../models/interview';

@Component({
  selector: 'app-interview-list',
  templateUrl: './interview-list.component.html',
  styleUrls: ['./interview-list.component.css']
})
export class InterviewListComponent implements OnInit {

  upcomingInterviewList: Interview[] = [];

  constructor(private interviewService: InterviewService) { }

  ngOnInit(): void {
    this.getUpcomingInterviews();
  }

  // Get a list of the upcoming interviews
  getUpcomingInterviews(): void {
    this.interviewService.getUpcomingInterviews().subscribe(upcomingInterviews => {
      this.upcomingInterviewList = upcomingInterviews;
    });
  }

}

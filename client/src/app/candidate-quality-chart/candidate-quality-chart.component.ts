import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { MetricService } from '../metric.service';
import { QualityMetric } from '../models/quality-metric';

@Component({
  selector: 'app-candidate-quality-chart',
  templateUrl: './candidate-quality-chart.component.html',
  styleUrls: ['./candidate-quality-chart.component.css']
})
export class CandidateQualityChartComponent implements OnInit {

  qualityMetric: QualityMetric;

  pieChartOptions: ChartOptions = {
    responsive: true,
  };
  pieChartLabels: Label[] = ['High', 'Average', 'Low'];
  pieChartData: SingleDataSet = [];
  pieChartType: ChartType = 'pie';
  pieChartLegend = true;
  pieChartPlugins = [];

  constructor(private metricService: MetricService) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();

    this.pieChartData = [0, 0, 0];
   }

  ngOnInit(): void {
    this.getCandidateQualityMetric(30);
  }

  // Get new candidate quality metric and update the chart
  getCandidateQualityMetric(days: number): void {
    this.metricService.getCandidateQuality(days).subscribe(qualityMetric => {
      this.pieChartData = [qualityMetric.high, qualityMetric.average, qualityMetric.low];
    });
  }

  // Respond to the user changing the time frame window by pulling new metrics
  onChangedTimeWindow(timeWindow: number): void {
    this.getCandidateQualityMetric(timeWindow);
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateQualityChartComponent } from './candidate-quality-chart.component';

describe('CandidateQualityChartComponent', () => {
  let component: CandidateQualityChartComponent;
  let fixture: ComponentFixture<CandidateQualityChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CandidateQualityChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateQualityChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { BaseChartDirective, Color, Label, SingleDataSet } from 'ng2-charts';
import { MetricService } from '../metric.service';

@Component({
  selector: 'app-interview-count-chart',
  templateUrl: './interview-count-chart.component.html',
  styleUrls: ['./interview-count-chart.component.css']
})
export class InterviewCountChartComponent implements OnInit {

  lineChartData: ChartDataSets[] = [];
  lineChartLabels: Label[] = [];
  lineChartOptions: ChartOptions = {
    responsive: true,
  };
  lineChartColors: Color[] = [];
  lineChartLegend = false;
  lineChartType: ChartType = 'line';
  lineChartPlugins = [];

  constructor(private metricsService: MetricService) { }

  ngOnInit(): void {
    this.getinterviewCountMetric(30);
  }

  // Get new interview count metric and update the chart
  getinterviewCountMetric(days: number): void {
    this.metricsService.getInterviewCount(days).subscribe(interviewCountMetric => {
      this.lineChartData.pop();
      this.lineChartData.push({ data: interviewCountMetric.counts});
      this.lineChartLabels = interviewCountMetric.dates;
    });
  }

  // Respond to the user changing the time frame window by pulling new metrics
  onChangedTimeWindow(timeWindow: number): void {
    this.getinterviewCountMetric(timeWindow);
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterviewCountChartComponent } from './interview-count-chart.component';

describe('InterviewCountChartComponent', () => {
  let component: InterviewCountChartComponent;
  let fixture: ComponentFixture<InterviewCountChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InterviewCountChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterviewCountChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

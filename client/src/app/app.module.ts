import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewHireCountComponent } from './new-hire-count/new-hire-count.component';
import { CandidateQualityChartComponent } from './candidate-quality-chart/candidate-quality-chart.component';
import { InterviewCountChartComponent } from './interview-count-chart/interview-count-chart.component';
import { TimeWindowComponent } from './time-window/time-window.component';

import { ChartsModule } from 'ng2-charts';
import { HttpClientModule } from '@angular/common/http';
import { InterviewListComponent } from './interview-list/interview-list.component';

@NgModule({
  declarations: [
    AppComponent,
    NewHireCountComponent,
    CandidateQualityChartComponent,
    InterviewCountChartComponent,
    TimeWindowComponent,
    InterviewListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

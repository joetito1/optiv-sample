import { Component, EventEmitter, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-time-window',
  templateUrl: './time-window.component.html',
  styleUrls: ['./time-window.component.css']
})
export class TimeWindowComponent implements OnInit {

  @Output() changedTimeWindow = new EventEmitter<number>();

  currentSelectedDays = 30;
  days: number[] = [30, 60, 90, 120];

  constructor() { }

  ngOnInit(): void {
    this.changedTimeWindow.emit(this.currentSelectedDays);
  }

  // Set the number of days of data to display
  setTimeWindow(selectedDays: number): void {
    this.currentSelectedDays = selectedDays;
    this.changedTimeWindow.emit(this.currentSelectedDays);
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeWindowComponent } from './time-window.component';

describe('TimeWindowComponent', () => {
  let component: TimeWindowComponent;
  let fixture: ComponentFixture<TimeWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimeWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
